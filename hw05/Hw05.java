// The purpose of this program is to generate a specific amount of user generated poker hands and to display the probability of each of the four types of pairs
// Luke Lenny
import java.util.Scanner;
public class pokerProbabilities{
  public static void main (String [] args){
    Scanner reader = new Scanner(System.in);
    // creates scanner variable
    
    int fourKindCount = 0;
    int threeKindCount = 0;
    int twoPairCount = 0;
    int onePairCount = 0;
    //creates counters for types of pairs
    
    System.out.print("Enter the number of hands you'd like to generate: ");
    int loops = reader.nextInt();
    //prompts the user for number of loops
    
    int counter = 0;
    while(counter < loops){
      
      int card1 = (int)(Math.random() * 52) + 1;
      
      int card2 = (int)(Math.random() * 52) + 1;
      while(true){
        if(card2 == card1){
          card2 = (int)(Math.random() * 52) + 1;
        }
        else{
          break;
        }
      }
      
      int card3 = (int)(Math.random() * 52) + 1;
      while(true){
        if(card3 == card1 || card3 == card2){
          card3 = (int)(Math.random() * 52) + 1;
        }
        else{
          break;
        }
      }
 
      int card4 = (int)(Math.random() * 52) + 1;
      while(true){
        if(card4 == card1 || card4 == card2 || card4 == card3){
          card4 = (int)(Math.random() * 52) + 1;
        }
        else{
          break;
        }
      }
      
      int card5 = (int)(Math.random() * 52) + 1;
      while(true){
        if(card5 == card1 || card5 == card2 || card5 == card3 || card5 == card4){
          card5 = (int)(Math.random() * 52) + 1;
        }
        else{
          break;
        }
      }
      
      // these statements create the random card values and also use while loops to check to make sure none are the same
      
      //this if checks for 4 of a kinds
      if(card1 % 13 == card2 % 13 && card1 % 13 == card3 % 13 && card1 % 13 == card4 % 13 ||
        card1 % 13 == card2 % 13 && card1 % 13 == card3 % 13 && card1 % 13 == card5 % 13 ||
        card1 % 13 == card2 % 13 && card1 % 13 == card4 % 13 && card1 % 13 == card5 % 13 ||
        card1 % 13 == card3 % 13 && card1 % 13 == card4 % 13 && card1 % 13 == card5 % 13 ||
        card2 % 13 == card3 % 13 && card2 % 13 == card4 % 13 && card2 % 13 == card5 % 13){
          fourKindCount += 1;
      }
      // this if checks for 3 of a kinds
      if(card1 % 13 == card2 % 13 && card1 % 13 == card3 % 13 ||
        card1 % 13 == card2 % 13 && card1 % 13 == card4 %13 ||
        card1 % 13 == card2 % 13 && card1 % 13 == card5 %13 ||
        card1 % 13 == card3 % 13 && card1 % 13 == card4 %13 ||
        card1 % 13 == card3 % 13 && card1 % 13 == card5 %13 ||
        card1 % 13 == card4 % 13 && card1 % 13 == card5 %13 ||
        card2 % 13 == card3 % 13 && card2 % 13 == card4 %13 ||
        card2 % 13 == card3 % 13 && card2 % 13 == card5 %13 ||
        card2 % 13 == card4 % 13 && card2 % 13 == card5 %13 ||
        card3 % 13 == card4 % 13 && card3 % 13 == card5 %13){
          threeKindCount +=1;
      }
      
      //this if checks for 1 pairs
      if(card1 % 13 == card2 % 13 ||
        card1 % 13 == card3 % 13 ||
        card1 % 13 == card4 % 13 ||
        card1 % 13 == card5 % 13 ||
        card2 % 13 == card3 % 13 ||
        card2 % 13 == card4 % 13 ||
        card2 % 13 == card5 % 13 ||
        card3 % 13 == card4 % 13 ||
        card3 % 13 == card5 % 13 ||
        card4 % 13 == card5 % 13){
          onePairCount +=1;
      }
      
      //this if checks for 2 pairs
      if(card1 % 13 == card2 % 13 && card3 % 13 == card4 % 13 ||
        card1 % 13 == card2 % 13 && card3 % 13 == card5 % 13 ||
        card1 % 13 == card2 % 13 && card4 % 13 == card5 % 13 ||
        card1 % 13 == card3 % 13 && card2 % 13 == card4 % 13 ||
        card1 % 13 == card3 % 13 && card3 % 13 == card5 % 13 ||
        card1 % 13 == card3 % 13 && card4 % 13 == card4 % 13 ||
        card1 % 13 == card4 % 13 && card2 % 13 == card3 % 13 ||
        card1 % 13 == card4 % 13 && card2 % 13 == card5 % 13 ||
        card1 % 13 == card4 % 13 && card3 % 13 == card5 % 13 ||
        card1 % 13 == card5 % 13 && card2 % 13 == card3 % 13 ||
        card1 % 13 == card5 % 13 && card2 % 13 == card4 % 13 ||
        card1 % 13 == card5 % 13 && card3 % 13 == card4 % 13 ||
        card2 % 13 == card3 % 13 && card4 % 13 == card5 % 13 ||
        card2 % 13 == card4 % 13 && card3 % 13 == card5 % 13 ||
        card2 % 13 == card5 % 13 && card4 % 13 == card3 % 13){
          twoPairCount += 1;
      }
          
        
      counter++;
    }
    
    //calculates the probability of each pairing
    double fourKindProb = (double) fourKindCount / (double) loops;
    double threeKindProb = (double) threeKindCount / (double) loops;
    double onePairProb = (double) onePairCount / (double) loops;
    double twoPairProb = (double) twoPairCount / (double) loops;

    //prints probabilities to the screen rounded to decimals
    System.out.println("The number of loops: " + loops);
    System.out.print("The probability of a Four-of-a-kind: ");
    System.out.printf("%.2f", fourKindProb);
    System.out.println("");
    System.out.print("The probability of a Three-of-a-kind: ");
    System.out.printf("%.2f", threeKindProb);
    System.out.println("");
    System.out.print("The probability of Two-pair: ");
    System.out.printf("%.2f", twoPairProb);
    System.out.println("");
    System.out.print("The probability of Two-pair: ");
    System.out.printf("%.2f",onePairProb);
    System.out.println("");

  }
}