//Luke Lenny
//the purpose of this program is to obtain info about a student's class and to check to make sure they're entering the correct types

import java.util.Scanner;
public class courseInfo{
  public static void main(String [] args){
    Scanner reader = new Scanner(System.in);
    //creates my scanner variable
    int courseNum = 0;
    String department = "";
    String dummy1 = "";
    int courseTimesPerWeek = 0;
    String dummy2 = "";
    int startTime = 0;
    String profName = "";
    int numStudents = 0;
    
    //this loop will run until the student enters an integer for their course number
    while(true){
      System.out.print("Please enter a course number: ");
      while(!reader.hasNextInt()){
        System.out.println("Incorrect Type. Use an Integer.");
        reader.next();
        System.out.print("Please enter a course number: ");
      }
    courseNum = reader.nextInt();
    dummy1 = reader.nextLine();
    break;
    }
    
    //this loop will run until the student enters the correct type for deparetment name
    while(true){
      System.out.print("Please enter a department name: ");
      while(!reader.hasNext()){
        System.out.println("Incorrect Type. Use a String.");
        reader.next();
        System.out.print("Please enter a department name: ");
      }
    department = reader.nextLine();
    break;
    }
    
    //this loop will run until the user enters an integer for the number of times met per week
    while(true){
      System.out.print("Please enter the amount of times per week a course meets: ");
      while(!reader.hasNextInt()){
        System.out.println("Incorrect Type. Use an Integer.");
        reader.next();
        System.out.print("Please enter the amount of times per week a course meets: ");
      }
    courseTimesPerWeek = reader.nextInt();
    break;
    }
    
    //this loop will run until the user enters a string telling what time the class starts
    while(true){
      System.out.print("Please enter the start time of your class: ");
      while(!reader.hasNextInt()){
        System.out.println("Incorrect Type. Use an Integer.");
        reader.next();
        System.out.print("Please enter the start time of your class:: ");
      }
    startTime = reader.nextInt();
    dummy2 = reader.nextLine();
    break;
    }
    
    //this loop will run until the user enters a string of the professor's name
    while(true){
      System.out.print("Please enter the name of your professor: ");
      while(!reader.hasNext()){
        System.out.println("Incorrect Type. Use a String.");
        reader.next();
        System.out.print("Please enter the name of your professor: ");
      }
    profName = reader.nextLine();
    break;
    }
    
    //this loop will run until the user enters an integer for the number of students in the class
    while(true){
      System.out.print("Please enter the number of students in class: ");
      while(!reader.hasNextInt()){
        System.out.println("Incorrect Type. Use an Integer.");
        reader.next();
        System.out.print("Please enter the number of students in class: ");
      }
    numStudents = reader.nextInt();
    break;
    }
    
    //prints all of the data the user entered in an organized fashion
    System.out.println("Course Information");
    System.out.println("Course number: " + courseNum);
    System.out.println("Department name: " + department);
    System.out.println("Number of times per week: " + courseTimesPerWeek);
    System.out.println("Start time: " + startTime);
    System.out.println("Professor's name: " + profName);
    System.out.println("Number of students: " + numStudents);
  }
}