// Luke Lenny
// this program takes die values and responds with the corressponding craps name using if statements

import java.util.Scanner;
public class CrapsIf{
  public static void main(String [] args){
    Scanner reader = new Scanner(System.in);
    //creates my scanner varibale
    
    //the next lines prompt the user ab what they wish to do and then move into conditionals based on their answer
    System.out.print("Would you like to cast two random die (enter 1) or enter two die (type 2): ");
    int castOrState = reader.nextInt();
    String slangName = "";
    
    if(castOrState == 1){
      // this conditional runs if the user wishes to cast two random die
      
      int die1 = (int)(Math.random() * 6) + 1;
      int die2 = (int)(Math.random() * 6) + 1;
      //creates values between 1 and 6 for each die
    
      System.out.println("You rolled a " + die1 + " and a " + die2);
      //displays the two die that were rolled
      
      // the following conditions cover every possibility created by the random values
      if(die1 == 1 && die2 == 1){
        slangName = "snake eyes";
      }
      else if((die1 == 1 && die2 == 2) || (die1 == 2 && die2 == 1)){
        slangName = "Ace Deuce";
      }
      else if((die1 == 1 && die2 == 3) || (die1 == 3 && die2 == 1)){
        slangName ="Easy Four";
      }
      else if((die1 == 1 && die2 == 4) || (die1 == 4 && die2 == 1) || (die1 == 2 && die2 == 3) || (die1 == 3 && die2 == 2)){
        slangName ="Fever Five";
      }
      else if((die1 == 1 && die2 == 5) || (die1 == 5 && die2 == 1) || (die1 == 4 && die2 == 2) || (die1 == 2 && die2 == 4)){
        slangName ="Easy Six";
      }
      else if((die1 == 1 && die2 == 6) || (die1 == 6 && die2 == 1) || (die1 == 2 && die2 == 5) || (die1 == 5 && die2 == 2) || (die1 == 3 && die2 == 4) || (die1 == 4 && die2 == 3)){
        slangName ="Seven Out";
      }
      else if(die1 == 2 && die2 == 2){
        slangName ="Hard Four";
      }
      else if((die1 == 6 && die2 == 2) || (die1 == 2 && die2 == 6) || (die1 == 3 && die2 == 5) || (die1 == 5 && die2 == 3)){
        slangName ="Easy Eight";
      }
      else if(die1 == 3 && die2 == 3){
        slangName ="Hard Six";
      }
      else if((die1 == 3 && die2 == 6) || (die1 == 6 && die2 == 3) || (die1 == 4 && die2 == 5) || (die1 == 5 && die2 == 4)){
        slangName = "Nine";
      }
      else if(die1 == 4 && die2 == 4){
        slangName = "Hard Eight";
      }
      else if((die1 == 4 && die2 == 6) || (die1 == 6 && die2 == 4)){
        slangName = "Easy Ten";
      }
      else if(die1 == 5 && die2 == 5){
        slangName = "Hard Ten";
      }
      else if((die1 == 5 && die2 == 6) || (die1 == 6 && die2 == 5)){
        slangName = "Yoleven";
      }
      else if(die1 == 6 && die2 == 6){
        slangName = "Boxcars";
      }
      System.out.println("You rolled a " + slangName);
    }
    else if(castOrState == 2){
      //this conditional runs if the user wants to enter values for die
      
      System.out.print("Enter the value of die 1: ");
      int userDie1 = reader.nextInt();
      System.out.print("Enter the value for die 2: ");
      int userDie2 = reader.nextInt();
      // asks the user for die values
      
      //the following code takes the user values and determines which slang name they have
      if(userDie1 == 1 && userDie2 == 1){
        slangName = "snake eyes";
      }
      else if((userDie1 == 1 && userDie2 == 2) || (userDie1 == 2 && userDie2 == 1)){
        slangName = "Ace Deuce";
      }
      else if((userDie1 == 1 && userDie2 == 3) || (userDie1 == 3 && userDie2 == 1)){
        slangName ="Easy Four";
      }
      else if((userDie1 == 1 && userDie2 == 4) || (userDie1 == 4 && userDie2 == 1) || (userDie1 == 2 && userDie2 == 3) || (userDie1 == 3 && userDie2 == 2)){
        slangName ="Fever Five";
      }
      else if((userDie1 == 1 && userDie2 == 5) || (userDie1 == 5 && userDie2 == 1) || (userDie1 == 4 && userDie2 == 2) || (userDie1 == 2 && userDie2 == 4)){
        slangName ="Easy Six";
      }
      else if((userDie1 == 1 && userDie2 == 6) || (userDie1 == 6 && userDie2 == 1) || (userDie1 == 2 && userDie2 == 5) || (userDie1 == 5 && userDie2 == 2) || (userDie1 == 3 &&userDie2 == 4) || (userDie1 == 4 && userDie2 == 3)){
        slangName ="Seven Out";
      }
      else if(userDie1 == 2 && userDie2 == 2){
        slangName ="Hard Four";
      }
      else if((userDie1 == 6 && userDie2 == 2) || (userDie1 == 2 && userDie2 == 6) || (userDie1 == 3 && userDie2 == 5) || (userDie1 == 5 && userDie2 == 3)){
        slangName ="Easy Eight";
      }
      else if(userDie1 == 3 && userDie2 == 3){
        slangName ="Hard Six";
      }
      else if((userDie1 == 3 && userDie2 == 6) || (userDie1 == 6 && userDie2 == 3) || (userDie1 == 4 && userDie2 == 5) || (userDie1 == 5 && userDie2 == 4)){
        slangName = "Nine";
      }
      else if(userDie1 == 4 && userDie2 == 4){
        slangName = "Hard Eight";
      }
      else if((userDie1 == 4 && userDie2 == 6) || (userDie1 == 6 && userDie2 == 4)){
        slangName = "Easy Ten";
      }
      else if(userDie1 == 5 && userDie2 == 5){
        slangName = "Hard Ten";
      }
      else if((userDie1 == 5 && userDie2 == 6) || (userDie1 == 6 && userDie2 == 5)){
        slangName = "Yoleven";
      }
      else if(userDie1 == 6 && userDie2 == 6){
        slangName = "Boxcars";
      }
      System.out.println("You rolled a " + slangName);
    }
    else{
      System.out.println("You didn't enter 1 or 2!");
    }
  }
}