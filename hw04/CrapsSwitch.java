// Luke Lenny
// this program takes die values and responds with the corressponding craps name using switch statements

import java.util.Scanner;
public class CrapsSwitch{
  public static void main(String [] args){
    Scanner reader = new Scanner(System.in);
    //creates my scanner varibale
    
    //the next lines prompt the user ab what they wish to do and then move into conditionals based on their answer
    System.out.print("Would you like to cast two random die (enter 1) or enter two die (type 2): ");
    int castOrState = reader.nextInt();
    String slangName = "";
    
    if(castOrState == 1){
      // this conditional runs if the user wishes to cast two random die
      
      int die1 = (int)(Math.random() * 6) + 1;
      int die2 = (int)(Math.random() * 6) + 1;
      //creates values between 1 and 6 for each die
    
      System.out.println("You rolled a " + die1 + " and a " + die2);
      //displays the two die that were rolled
      
      // the following conditions cover every possibility created by the random values
      switch(die1){
         case 1:
            switch(die2){
              case 1:
                  slangName = "snakeEyes";
                  break;
              case 2:
                  slangName = "Ace Duce";
                  break;
              case 3:
                  slangName = "Easy Four";
                  break;
              case 4:
                  slangName = "Fever Five";
                  break;
              case 5:
                  slangName = "Easy Six";
                  break;
              case 6:
                  slangName = "Seven Out";
                  break;
            }
           break;
        case 2:
            switch(die2){
              case 1:
                  slangName = "Ace Deuce";
                  break;
              case 2:
                  slangName = "Hard Four";
                  break;
              case 3:
                  slangName = "Fever Five";
                  break;
              case 4:
                  slangName = "Easy Six";
                  break;
              case 5:
                  slangName = "Seven Out";
                  break;
              case 6:
                  slangName = "Easy Eight";
                  break;
            }
           break;
        case 3:
            switch(die2){
              case 1:
                  slangName = "Easy Four";
                  break;
              case 2:
                  slangName = "Fever Five";
                  break;
              case 3:
                  slangName = "Hard Six";
                  break;
              case 4:
                  slangName = "Seven Out";
                  break;
              case 5:
                  slangName = "Easy Eight";
                  break;
              case 6:
                  slangName = "Nine";
                  break;
            }
           break;
        case 4:
            switch(die2){
              case 1:
                  slangName = "Fever Five";
                  break;
              case 2:
                  slangName = "Easy Six";
                  break;
              case 3:
                  slangName = "Seven Out";
                  break;
              case 4:
                  slangName = "Hard Eight";
                  break;
              case 5:
                  slangName = "Nine";
                  break;
              case 6:
                  slangName = "Easy Ten";
                  break;
            }
           break;
        case 5:
            switch(die2){
              case 1:
                  slangName = "Easy Six";
                  break;
              case 2:
                  slangName = "Seven Out";
                  break;
              case 3:
                  slangName = "Easy Eight";
                  break;
              case 4:
                  slangName = "Nine";
                  break;
              case 5:
                  slangName = "Hard Ten";
                  break;
              case 6:
                  slangName = "Yo-leven";
                  break;
            }
           break;
        case 6:
            switch(die2){
              case 1:
                  slangName = "Seven Out";
                  break;
              case 2:
                  slangName = "Easy Eight";
                  break;
              case 3:
                  slangName = "Nine";
                  break;
              case 4:
                  slangName = "Easy Ten";
                  break;
              case 5:
                  slangName = "Yo-leven";
                  break;
              case 6:
                  slangName = "Boxcars";
                  break;
            }
           break;  
      }
      
      System.out.println("You rolled a " + slangName);
    }
    else if(castOrState == 2){
      //this conditional runs if the user wants to enter values for die
      
      System.out.print("Enter the value of die 1: ");
      int userDie1 = reader.nextInt();
      System.out.print("Enter the value for die 2: ");
      int userDie2 = reader.nextInt();
      // asks the user for die values
      
      //the following code takes the user values and determines which slang name they have
       switch(userDie1){
         case 1:
            switch(userDie2){
              case 1:
                  slangName = "snakeEyes";
                  break;
              case 2:
                  slangName = "Ace Duce";
                  break;
              case 3:
                  slangName = "Easy Four";
                  break;
              case 4:
                  slangName = "Fever Five";
                  break;
              case 5:
                  slangName = "Easy Six";
                  break;
              case 6:
                  slangName = "Seven Out";
                  break;
            }
           break;
        case 2:
            switch(userDie2){
              case 1:
                  slangName = "Ace Deuce";
                  break;
              case 2:
                  slangName = "Hard Four";
                  break;
              case 3:
                  slangName = "Fever Five";
                  break;
              case 4:
                  slangName = "Easy Six";
                  break;
              case 5:
                  slangName = "Seven Out";
                  break;
              case 6:
                  slangName = "Easy Eight";
                  break;
            }
           break;
        case 3:
            switch(userDie2){
              case 1:
                  slangName = "Easy Four";
                  break;
              case 2:
                  slangName = "Fever Five";
                  break;
              case 3:
                  slangName = "Hard Six";
                  break;
              case 4:
                  slangName = "Seven Out";
                  break;
              case 5:
                  slangName = "Easy Eight";
                  break;
              case 6:
                  slangName = "Nine";
                  break;
            }
           break;
        case 4:
            switch(userDie2){
              case 1:
                  slangName = "Fever Five";
                  break;
              case 2:
                  slangName = "Easy Six";
                  break;
              case 3:
                  slangName = "Seven Out";
                  break;
              case 4:
                  slangName = "Hard Eight";
                  break;
              case 5:
                  slangName = "Nine";
                  break;
              case 6:
                  slangName = "Easy Ten";
                  break;
            }
           break;
        case 5:
            switch(userDie2){
              case 1:
                  slangName = "Easy Six";
                  break;
              case 2:
                  slangName = "Seven Out";
                  break;
              case 3:
                  slangName = "Easy Eight";
                  break;
              case 4:
                  slangName = "Nine";
                  break;
              case 5:
                  slangName = "Hard Ten";
                  break;
              case 6:
                  slangName = "Yo-leven";
                  break;
            }
           break;
        case 6:
            switch(userDie2){
              case 1:
                  slangName = "Seven Out";
                  break;
              case 2:
                  slangName = "Easy Eight";
                  break;
              case 3:
                  slangName = "Nine";
                  break;
              case 4:
                  slangName = "Easy Ten";
                  break;
              case 5:
                  slangName = "Yo-leven";
                  break;
              case 6:
                  slangName = "Boxcars";
                  break;
            }
           break;  
      }
      System.out.println("You rolled a " + slangName);
  }
 }
}