import java.util.Scanner;
//the goal of this program is to take user inputs in the form of acres of land affected by rain and the rate of rain and to return the resulting cubic miles affected
public class Pyramid{
  
  public static void main (String [] args){
    Scanner reader = new Scanner(System.in);
    // creates my scanner object
    
    System.out.print("Enter the length of the square side of the pyramid: ");
    double squareSide = reader.nextDouble();
    System.out.print("Enter the height of the pyramid: ");
    double height = reader.nextDouble();
    //prompts the user for the two values needed for the caluclation and stores them in variables
    
    double volume = (squareSide * squareSide * height) / 3;
    System.out.println("The volume of this pyramid is " +volume);
  }
}