import java.util.Scanner;
//the goal of this program is to take user inputs in the form of acres of land affected by rain and the rate of rain and to return the resulting cubic miles affected
public class Convert{
  
  public static void main (String [] args){
    Scanner reader = new Scanner(System.in);
    // creates my scanner object
    
    System.out.print("Enter the affected area in acres: ");
    double acres = reader.nextDouble();
    System.out.print("Enter the rainfall in the affected area: ");
    double rainfall = reader.nextDouble();
    //asks for and stores the two inputs from the user
    
    double gallons = acres * rainfall * 27154;
    // one inch of rain on 1 acre is equal to 27154 gallons
    double cubicMiles = 9.08169e-13 * gallons;
    //converts gallons to cubic miles by multiplying by the conversion factor
    
    System.out.println("The amount of rain in cubic miles is "+cubicMiles);
    //prints the calculated value to the screen
    
  }
}