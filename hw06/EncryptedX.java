//Luke Lenny
//The purpose of this program is to print a hidden X in a field of asterisks given a user input dimensions
import java.util.Scanner;

public class EncryptedX{
	public static void main(String []args){

		Scanner reader = new Scanner(System.in);
		//creates a scanner variable

		int rows = 0;

		//validates that the input is an integer
		System.out.print("Enter the number of rows in your table from 1 - 100: ");
      	while(!reader.hasNextInt()){

       		System.out.println("Incorrect Type. Use an Integer.");
       		reader.next();
       		System.out.print("Please enter an integer: ");
      	}
    	rows = reader.nextInt();

    	//checks to make sure the user entered a value in the bounds
    	while(true){
    		if( rows < 0 || rows > 100){
    			System.out.println("You entered a number out of range, enter an integer between 1-10: ");
    			rows = reader.nextInt();
    		}
    		else{
    			break;
    		}
    	}

    	//creates the x
    	for(int i = 1; i < rows + 1; i++){
    		for(int j = 1; j < rows + 1; j++){
    			if(j == i || j == (rows + 1) - i){
    				System.out.print(" ");
    			}
    			else{
    				System.out.print("*");
    			}
    		}
    		System.out.println("");
    	}
	}
}