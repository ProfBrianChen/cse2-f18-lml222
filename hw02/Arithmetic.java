//the goal of this program is to caluclate your total cost while shopping including the PA sales tax of 6%
// the program will return the price of each of the clothing items without tax, the amount of tax on each, and the total cost after tax
public class Arithmetic{
  
  public static void main (String [] args){
     //Number of pairs of pants
     int numPants = 3;
     //Cost per pair of pants
     double pantsPrice = 34.98;
    
     //Number of sweatshirts
     int numShirts = 2;
     //Cost per shirt
     double shirtPrice = 24.99;
    
     //Number of belts
     int numBelts = 1;
     //cost per belt
     double beltPrice = 33.99;
    
     //the tax rate
     double paSalesTax = 0.06;
    
     double totalCostOfPants = (numPants * pantsPrice);   //total cost of pants
     double totalCostOfSweatshirts = (numShirts * shirtPrice);   //total cost of sweatshirts
     double totalCostOfBelts = (numBelts * beltPrice);   //total cost of belts
     
     System.out.println("-----------------------------------------------------------------------");
     //this print statements is purely for stylistic purposes in the terminal output
    
     //the following print statements print out the total cost of pants, sweatshirts, and belts WITHOUT tax
     System.out.println("The total cost of pants before tax is $" + totalCostOfPants + ".");
     System.out.println("The total cost of shirts before tax is $" + totalCostOfSweatshirts + ".");
     System.out.println("The total cost of belts before tax is $" + totalCostOfBelts + ".");
     
     double totalCostWithoutTax = totalCostOfPants + totalCostOfSweatshirts + totalCostOfBelts;
     //calculates the total purchase cost WITHOUT tax
     System.out.println("The total cost of the entire purchase without tax is $" + totalCostWithoutTax);
     //prints the total purchase cost WITHOUT tax
     System.out.println("");
     //creates stylistic line break
    
     double totalTaxOnPants = totalCostOfPants * paSalesTax;
     //the dollar amount of tax paid on pants
     double totalTaxOnShirts = totalCostOfSweatshirts * paSalesTax;
     //the dollar amount of tax paid on sweatshirts
     double totalTaxOnBelts = totalCostOfBelts * paSalesTax;
     //the dollar amount of tax paid on pants
    
     //the following calculations will round the tax values to two decimals
     totalTaxOnPants *= 100;
     totalTaxOnPants = (int) totalTaxOnPants;
     totalTaxOnPants /= 100;
    
     totalTaxOnShirts *= 100;
     totalTaxOnShirts = (int) totalTaxOnShirts;
     totalTaxOnShirts /= 100;
    
     totalTaxOnBelts *= 100;
     totalTaxOnBelts = (int) totalTaxOnBelts;
     totalTaxOnBelts /= 100;
    
     //the following print statments will print out the total dollar amount of tax on pants, sweatshirts and belts, followed by a line break
     System.out.println("The total dollar amount of taxes on pants is $" + totalTaxOnPants);
     System.out.println("The total dollar amount of taxes on pants is $" + totalTaxOnShirts);
     System.out.println("The total dollar amount of taxes on pants is $" + totalTaxOnBelts);
     System.out.println("");
     
     //the following calculations will yiled the total cost of pants, sweatshirts, and belts after tax, as well as the total cost of the entire purchase after tax
     double afterTaxCostPants = totalCostOfPants + totalTaxOnPants;
     double afterTaxCostShirts = totalCostOfSweatshirts + totalTaxOnShirts;
     double afterTaxCostBelts = totalCostOfBelts + totalTaxOnBelts;
     double grandTotal = afterTaxCostBelts + afterTaxCostShirts + afterTaxCostPants;
    
     //the following calculations round the grand total to 2 decimals
     grandTotal *= 100;
     grandTotal = (int) grandTotal;
     grandTotal /= 100;
    
     //the following print statements display the total cost of pants, shirts, and belts after tax, as well as the grand total
     System.out.println("The total cost of pants after tax is $" + afterTaxCostPants);
     System.out.println("The total cost of sweatshirts after tax is $" + afterTaxCostShirts);
     System.out.println("The total cost of belts after tax is $" + afterTaxCostBelts);
     System.out.println("The grand total after tax is $" + grandTotal);
    
     //the following print statement is for stylistic purposes in the terminal output
     System.out.println("-----------------------------------------------------------------------");
  }
}