// The goal of this program is to achieve the following goals: to print the number
// of minutes for each trip, the number of counts for each trip, the distance of
// each trip in miles, and the distance of the two trips combined
//
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
    int secsTrip1=480;  //the amount of seconds for the first trip
    int secsTrip2=3220;  //the amount of seconds for the second trip
		int countsTrip1=1561;  //the number of wheel rotations in the first trip
		int countsTrip2=9037; //the number of wheel rotations in the second trip
    double wheelDiameter=27.0;  //the diameter of the bike wheel
  	double PI=3.14159; //the value used for pi
  	double feetPerMile=5280;  //the nubmer of feet in one mile
  	double inchesPerFoot=12;   //the amount of inches in a foot
  	double secondsPerMinute=60;  //the amount of seconds in a minute
	  double distanceTrip1, distanceTrip2,totalDistance; //the values that we will be calculating and returning in the program
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");

    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
     
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    //repeats the same calculations from trip 1 but instead caluclates it in a single line
	  totalDistance=distanceTrip1+distanceTrip2;
    //adds the two distances together, yielding the total distance
    
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");
    //these print statements display the distance of each trip, as well as the distance of both combined.
	}  //end of main method   
} //end of class
