//Luke Lenny
//the purpose of this program is to search a sorted array using binary search and a scrambled array using linear search

import java.util.Random;
import java.util.Scanner;

public class CSE2Linear{
	public static void binarySearch(int [] list, int goalValue){
		int start = 0;
		int end = list.length - 1;
		int iterations = 0;
		int middle = 0;

		while(start <= end){
			iterations++;
			middle = (start + end) / 2;
			if(list[middle] < goalValue){
				start = middle + 1;
			}
			else if(list[middle] > goalValue){
				end = middle - 1;
			}
			else{
				System.out.println(goalValue + " was found after " + iterations + " iterations.");
				return;
			}
		}
		System.out.println(goalValue + " was not found after " + iterations + " iterations.");
		return;
	}

	public static void randomizer(int [] list){
		Random generator = new Random();
		int rand1 = 0;
		int rand2 = 0;
		int temp = 0;

		//randomizes the array
		for(int i = 0; i < 100; i++){
			rand1 = generator.nextInt(15);
			rand2 = generator.nextInt(15);

			temp = list[rand1];
			list[rand1] = list[rand2];
			list[rand2] = temp;
		}

		//prints out randomized array
		for(int i = 0; i < list.length; i++){
			System.out.print(list[i] + " ");
		}
		System.out.println("");
	}

	public static void linearSearch(int [] list, int search){
		int iterations = 0;

		for(int i = 0; i < list.length; i++){
			iterations++;
			if(list[i] == search){
				System.out.println(search + " was found after " + iterations + " iterations.");
				return;
			}
		}
		System.out.println(search + " was not found after " + iterations + " iterations.");
		return;
	}

	public static void main(String [] args){
		Scanner reader = new Scanner(System.in);
		int n = 0;
		int search1 = 0;
		int search2 = 0;
		int [] grades = new int [15];

		System.out.print("Enter a value: ");
		while(!reader.hasNextInt()){
			System.out.print("Wrong type, enter an integer: ");
			reader.next();	
		}
		int firstNum = reader.nextInt();
		grades[0] = firstNum;

		for(int i = 1; i < grades.length; i++){

			System.out.print("Enter a value greater than the previous: ");

			//checks to see if input is an int
			while(!reader.hasNextInt()){
				System.out.print("Wrong type, enter an integer: ");
				reader.next();	
			}
			n = reader.nextInt();

			//checks to see if value is greater than previous vale
			while(n < 0 || n > 100){
				System.out.print("Error. Please enter a value in the range of 1-100: ");
				n = reader.nextInt();

				//checks to see if in range
				while(n <= grades[i - 1]){
					System.out.print("Error. Please enter a value greater than the previous: ");
					n = reader.nextInt();
				}
			}
			grades[i] = n;
		}

		for(int i = 0; i < grades.length; i++){
			System.out.print(grades[i] + " ");
		}
		System.out.println("");

		System.out.print("(Binary Search) Enter a grade to search for: ");
		search1 = reader.nextInt();
		binarySearch(grades, search1);
		
		randomizer(grades);
		System.out.print("(Linear Search) Enter a grade to search for: ");
		search2 = reader.nextInt();
		linearSearch(grades, search2);
	}
}