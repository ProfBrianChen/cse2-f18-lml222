//Luke Lenny
//the purpose of this program is to take a sample string and perform va 
import java.util.Scanner;
public class hw07{

	public static String sampleText(){
		Scanner reader = new Scanner(System.in);

		//prompts user for sample text and then returns it as a string
		System.out.println("Enter a sample text: ");
		String sample = reader.nextLine();
		return sample;
	}
	public static String printMenu(){
		Scanner reader = new Scanner(System.in);
		String input = "";
		//the following print statements display the menu items
		System.out.println("");
		System.out.println("MENU");
		System.out.println("c - Number of non-whitespace characters");
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replace all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - Quit");
		System.out.println("");
		System.out.print("Choose an option: ");
		input = reader.nextLine();

		//continues to ask the user to select an option until they enter a valid character
		while(true){

			if(input.equals("c")){
				return "c";
			}
			else if(input.equals("w")){
				return "w";
			}
			else if(input.equals("f")){
				return "f";
			}
			else if(input.equals("r")){
				return "r";
			}
			else if(input.equals("s")){
				return "s";
			}
			else if(input.equals("q")){
				return "q";
			}
			else{
				System.out.println("Not a viable input.");
				System.out.print("Choose an option: ");
				input = reader.nextLine();
			}
		}

	}
	public static int getNumOfNonWSCharacters(String s){
		int numSpaces = 0;
		int numReturned = 0;
		//loop iterates through the entire string
		for(int i = 0; i < s.length(); i++){
			//checks to see if the character at position i is a space char
			if(s.charAt(i) == ' '){
				numSpaces = numSpaces + 1;
			}
			else{
				numReturned = numReturned + 1; 
			}
		}
		return numReturned;
	}
	public static int getNumOfWords(String s){
		//loop iterates through the entire string
		int numWords = 0;

		//these lines use the shortenSpace method so the method doesnt think groups of spaces are words
		String editedString = s;
		editedString = shortenSpace(editedString);

		for(int i = 0; i < editedString.length(); i++){
			//adds one to the numWords counter if the next character is a space
			if(editedString.charAt(i) == ' '){
				numWords = numWords + 1;;
			}
		}
		//accounts for the last word which doesnt have a space after it
		numWords = numWords + 1;
		return numWords;
	}
	public static int findText(String lookingFor, String sample){
		int returnedValue = 0;
		int counter = 0;

		if(sample.contains(lookingFor)){
			returnedValue = -1; //accounts for the last instance through the 
			while(counter != -1){
				counter = sample.indexOf(lookingFor, counter + 1);
				returnedValue++;
			}
		}
		return returnedValue;
	}
	public static String replaceExclamation(String s){
		//replaces every ! with a .
		String newString = s.replace("!", ".");
		return newString;
	}
	public static String shortenSpace(String s){
		//replaces all instances is of more than one space with one space
		String newString = s.replaceAll(" +", " ");
		return newString;
	}
	public static void main(String [] args){
		Scanner reader = new Scanner(System.in);
		
		int nonWhiteChars = 0;
		int numWords = 0;
		int numFoundWords = 0;
		String searchedWord = " ";
		String exclamationReplacedString = " ";
		String doubleSpaceReplacedString = " ";

		//prints out the sample text the user enters
		String sample = sampleText();
		System.out.println("You entered: ");
		System.out.println(sample);

		//calls the print menu method
		String menuChoice = printMenu();
		while(true){
			if(menuChoice.equals("c")){
				nonWhiteChars = getNumOfNonWSCharacters(sample);
				System.out.println("Number of non-whitespace characters: " + nonWhiteChars);
			}
			else if(menuChoice.equals("w")){
				numWords = getNumOfWords(sample);
				System.out.println("Number of words: " + numWords);
			}
			else if(menuChoice.equals("f")){
				System.out.println("Etner the phrase you would like to search for: ");
				searchedWord = reader.nextLine();

				numFoundWords = findText(searchedWord, sample);
				System.out.println("You used -" + searchedWord + "- " +  numFoundWords + " times.");
			}
			else if(menuChoice.equals("r")){
				exclamationReplacedString = replaceExclamation(sample);
				System.out.println("Edited text: " + exclamationReplacedString);
			}
			else if(menuChoice.equals("s")){
				doubleSpaceReplacedString = shortenSpace(sample);
				System.out.println("Edited text: " + doubleSpaceReplacedString);
			}
			else if(menuChoice.equals("q")){
				System.out.println("Goodbye!");
				break;
			}
			//continues asking the user for another menu option until they chose q
			menuChoice = printMenu();
		}
	}
}