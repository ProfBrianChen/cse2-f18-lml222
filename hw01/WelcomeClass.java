public class WelcomeClass{
  
  public static void main (String [] args){
    // first three print statements create the welcome banner
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    // next five print statements create the border for my 6 digit lehigh ID
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-L--M--L--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    // this last print statement displays my short autobiographical sentence                   
    System.out.println("My name is Luke Lenny, I grew up in Southern Jersey, and my favorite subject is computer science.");
  }
}