//Luke Lenny
//The purpose of this program is to take user 
import java.util.Scanner;
public class PatternC{
	public static void main(String [] args){
		//creates scanner variable
		Scanner reader = new Scanner(System.in);

		//checks to make sure the type is correct
		int numRows = 0;
		while(true){
      		System.out.print("Please enter an Integer between 1 and 10: ");
      		while(!reader.hasNextInt()){
        		System.out.println("Incorrect Type. Use an Integer.");
        		reader.next();
        		System.out.print("Please enter an integer: ");
      		}
    		numRows = reader.nextInt();

    		//checks to make sure pyramid length is within bounds
    		while(true){
    			if( numRows < 1 || numRows > 10){
    				System.out.println("You entered a number out of range, enter an integer between 1-10: ");
    				numRows = reader.nextInt();
    			}
    			else{
    				break;
    			}
    		}
    		//creates the pyramid
    		for(int i = 1; i < numRows + 1; i++){

                for(int o = 1; o<= numRows - i; o++){
                    System.out.print(" ");
                }
                
    			for(int j = i; j > 0; j--){
    				System.out.print(j);

    			}
    			System.out.println("");
    		}

    	break;
    	}
	}
}