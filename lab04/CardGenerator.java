// the purpose of this program is to generator a random card from a standard 52 card deck
public class CardGenerator{
  public static void main (String [] args){
    int cardNum = (int)(Math.random()*51) + 1;
    // creates a random number from 1 to 52
    String suit = "";
    String identity = "";
    
    switch (cardNum % 13){
      case 0: identity = "Ace";
    break;
      case 1: identity = "Two";
    break;
      case 2: identity = "Three";
    break;
      case 3: identity = "Four";
    break;
      case 4: identity = "Five";
    break;
      case 5: identity = "Six";
    break;
      case 6: identity = "Seven";
    break;
      case 7: identity = "Eight";
    break;
      case 8: identity = "Nine";
    break;
      case 9: identity = "Ten";
    break;
      case 10: identity = "Jack";
    break;
      case 11: identity = "Queen";
    break;
      case 12: identity = "King";
    break;
    }
    // a series of switch statements used to determine the identity of the card 
    
    if(cardNum <= 13){
      suit ="Diamonds";
    }
    else if(cardNum <= 26){
      suit = "Clubs";
    }
    else if(cardNum <= 38){
      suit = "Hearts";
    }
    else{
      suit = "Spades";
    }
    //4 if conditionals used to determine the suit of the card
    System.out.println(identity + " of " + suit);
  }
}