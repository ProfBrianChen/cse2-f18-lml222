//Luke Lenny
//The purpose of this program is to munipulate a deck of cards and return a shuffled deck
import java.util.Random;
import java.util.Scanner;
public class Shuffling{

	public static void printArray(String [] list){
		for(int i = 0; i < list.length; i++){
			//goes through every item in the array and prints it followed by a space
			System.out.print(list[i] + " ");
		}
	}
	public static String[] shuffle(String [] list){
		Random gen = new Random();
		int swap1 = 0;
		int swap2 = 0;
		String temp = " ";

		for(int i = 0; i < 1000; i++){
			swap1 = gen.nextInt(51);
			swap2 = gen.nextInt(51);
			
			//assigns the first value to a temp before switching the values
			temp = list[swap1];
			list[swap1] = list[swap2];
			list[swap2] = temp;
		}
		return list;
	}
	public static String[] getHand(String [] list, int index, int numCards){
		String [] hand = new String [numCards];
		
		for(int i = 0; i < numCards; i++){
			if(index >= 0){
				hand[i] = list[index];
			}
			else{
				//makes new deck
				shuffle(list);
				//reset index
				index = 52;
				//prevents the waste of first card
				i--;
			}
			index --;
			
		}
		return hand;
	}

	public static void main(String[] args) { 
		Scanner scan = new Scanner(System.in); 
 		//suits club, heart, spade or diamond 

		String[] suitNames={"C","H","S","D"};    
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
		String[] cards = new String[52]; 
		String[] hand = new String[5];

		int numCards = 5; 
		int again = 1; 
		int index = 51;

		for (int i=0; i<52; i++){ 
  			cards[i]=rankNames[i%13]+suitNames[i/13]; 
		}

		printArray(cards); 
		shuffle(cards); 
		System.out.println("Shuffled");
		printArray(cards); 
		System.out.println("");

		while(again == 1){ 
   			hand = getHand(cards,index,numCards); 
   			printArray(hand);
   			System.out.println("");
   			index = index - numCards;
   			System.out.print("Enter a 1 if you want another hand drawn: "); 
   			again = scan.nextInt(); 
		  }  
  	} 
}
